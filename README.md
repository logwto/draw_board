# draw_board

> Simple drawing board project based on zrender

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run e2e tests
npm run e2e

# run all tests
npm test

#说明
该项目基于zrender工具库实现简易的画板功能

主要练习使用zrender提供的方法

该项目功能并不严谨，建议参考学习绘图，不建议实际使用
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
